# Coveo Exercise

This project contains work related to the Coveo exercise (Technical Test - DBA, Data Engineering and Analytics).

The project also includes a devcontainer that should install all the necessary tools to run the provided code.

- The data for the exercise was loaded into Snowflake using the script `bin/load.sql`, and there are more related instructions below. (Note: I couldn't include the raw data files in this repository as there were some restrictions on file sizes on free GitLab accounts. Previously the `raw` directory in this project contained the data provided in the prompt, converted to parquet files).
- The answers to the 'SQL Requests' section of the exercise are located in the `analyses` directory. (Note: in some cases when I wasn't sure how to interpret the prompt or the data, I just made an assumption and documented it in the file).
- The "Shape the Data" section of the exercise was completed with dbt models located in the `models` directory. For this section I built a simple "user" type dimensional model, with attributes that summarize the user's different types of activity. The dbt project's structure follows [the layout from dbt-labs](https://docs.getdbt.com/best-practices/how-we-structure/1-guide-overview).

## Project setup

The project includes a devcontainer definition that will install the necessary dependencies.

After opening the project in VSCode, you should be prompted to build the devcontainer.

Once the devcontainer has finished building, you can activate the Python virtual environment that contains the dbt and Snowflake cli installation.

```
source .venv/bin/activate
```

Also you'll need to add the following target to your dbt profile's default profile at `~/.dbt/profiles.yml`.

```yml
default:
  outputs:
    dev:
      database: coveo
      schema: dev    
      type: snowflake
      account: ...
      user: ...
      password: ...
      warehouse: ...
      role: ...
  target: dev
```

To check that the project has been set up correctly run the `debug` command.

```
dbt debug
```

## Loading the Data

The data loading script can be ran with the Snowflake CLI.

To initialize the Snowflake CLI, create a new connection. The role used by the connection should have `CREATE DATABASE` permissions on the Snowflake instance.

```
snow connection add
```

Run the `bin/load.sql` script to build the raw data tables.

```
snow sql -f bin/load.sql
```

## Building the dbt models

To create the dbt models use the `build` command.

```
dbt build
```
