#!/bin/bash

apt update
apt install -y python3-dev python3-virtualenv git
python3 -m virtualenv .venv
.venv/bin/pip install -r requirements.txt
