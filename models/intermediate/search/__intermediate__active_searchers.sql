{{
    config(
        materialized='view',
        unique_key='user_id'
    )
}}

select
    user_id,
    count(1) as searches,
    count(distinct s.visit_id) as visits
from {{ ref('stg_coveo__searches') }} s
group by user_id