{{
    config(
        materialized='view',
        unique_key='user_id'
    )
}}

select
    user_id,
    count(1) as clicks,
    count(distinct s.visit_id) as visits_with_clicks
from {{ ref('stg_coveo__clicks') }} s
group by user_id