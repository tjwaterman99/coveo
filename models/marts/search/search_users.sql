{{
    config(
        materialized='table',
        unique_key='user_id'
    )
}}


select
    s.user_id,
    s.visits,
    s.searches,
    coalesce(c.clicks, 0) as clicks,
    coalesce(c.visits_with_clicks, 0) as visits_with_clicks
from {{ ref('__intermediate__active_searchers') }} s
left join {{ ref('__intermediate__active_clickers') }} c on s.user_id = c.user_id