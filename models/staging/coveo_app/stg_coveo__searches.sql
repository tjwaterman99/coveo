select 
    "id" as search_id,
    "datetime"::timestamp as searched_at,
    "withClicks"::boolean as with_clicks,
    "visitId" as visit_id,
    "userId" as user_id
from {{ source('coveo', 'searches') }}