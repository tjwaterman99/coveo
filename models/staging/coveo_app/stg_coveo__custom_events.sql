select 
    "customEventId" as event_id,
    "visitorId" as visitor_id,
    "visitId" as visit_id,
    -- Assuming it's the sent_at time of the event, could also be received_at, loaded_at, etc
    "datetime"::timestamp as sent_at
from {{ source('coveo', 'custom_events') }}