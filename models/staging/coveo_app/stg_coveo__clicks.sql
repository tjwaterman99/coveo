select 
    "clickId" as click_id,
    "searchId" as search_id,
    "datetime"::timestamp as clicked_at,
    "visitorId" as visitor_id,
    "visitId" as visit_id,
    "userId" as user_id,
    "sourceName" as source_name,
    "documentUrl" as document_url,
    "clickRank"::int as click_rank
from {{ source('coveo', 'clicks') }}