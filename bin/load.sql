create or replace database coveo;
create or replace schema coveo.raw;
create or replace file format coveo.raw.parquet_format with type = 'parquet';
create or replace stage coveo.raw.raw with file_format = coveo.raw.parquet_format;
put 'file://raw/*' @coveo.raw.raw auto_compress=false parallel=4 overwrite=True;

-- These statements generate an empty table with a schema that matches the
-- schema defined in each file. In the next loading step they'll be populated
-- with the actual data.
create or replace table coveo.raw.raw_clicks using template (
  select array_agg(object_construct(*))
    from table(
      infer_schema(
        location=>'@coveo.raw.raw/clicks.parquet'
        , file_format=>'parquet_format'
      )
    )
);

create or replace table coveo.raw.raw_custom_events using template (
  select array_agg(object_construct(*))
    from table(
      infer_schema(
        location=>'@coveo.raw.raw/custom_events.parquet'
        , file_format=>'parquet_format'
      )
    )
);

create or replace table coveo.raw.raw_groups using template (
  select array_agg(object_construct(*))
    from table(
      infer_schema(
        location=>'@coveo.raw.raw/groups.parquet'
        , file_format=>'parquet_format'
      )
    )
);

create or replace table coveo.raw.raw_keywords using template (
  select array_agg(object_construct(*))
    from table(
      infer_schema(
        location=>'@coveo.raw.raw/keywords.parquet'
        , file_format=>'parquet_format'
      )
    )
);

create or replace table coveo.raw.raw_searches using template (
  select array_agg(object_construct(*))
    from table(
      infer_schema(
        location=>'@coveo.raw.raw/searches.parquet'
        , file_format=>'parquet_format'
      )
    )
);

copy into raw_clicks from @coveo.raw.raw/clicks.parquet file_format = (format_name= 'parquet_format') match_by_column_name=case_insensitive;
copy into raw_custom_events from @coveo.raw.raw/custom_events.parquet file_format = (format_name= 'parquet_format') match_by_column_name=case_insensitive;
copy into raw_groups from @coveo.raw.raw/groups.parquet file_format = (format_name= 'parquet_format') match_by_column_name=case_insensitive;
copy into raw_keywords from @coveo.raw.raw/keywords.parquet file_format = (format_name= 'parquet_format') match_by_column_name=case_insensitive;
copy into raw_searches from @coveo.raw.raw/searches.parquet file_format = (format_name= 'parquet_format') match_by_column_name=case_insensitive;
