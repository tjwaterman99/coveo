-- List all visits with at least one click.

select 
    visit_id,
    count(1) as clicks,
    min(clicked_at) as first_clicked_at
from {{ ref('stg_coveo__clicks') }}
group by 1
-- This filter should not technically be necessary because the table contains 
-- only clicks, but I've included it just for illustration
having count(1) > 0 
order by 3 desc
