-- List the 10 most active users (by number of searches) having at least 1 click on a
-- document coming from a source starting with “Confluence”.

select
    user_id,
    count(1) as num_searches
from {{ ref('stg_coveo__searches') }} s
where exists(
    select 1 
    from {{ ref('stg_coveo__clicks')}} c
    where c.user_id = s.user_id 
    and lower(c.source_name) like 'confluence%'
)
group by 1
order by num_searches desc
limit 10