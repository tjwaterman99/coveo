-- The percentage of search having clicks per day, over the last 7 days, including
-- overall summary value (using a single SQL query, adding a column to produce
-- the summary value for the overall period)


-- NOTES:
-- I'm not 100% sure how to interpret the prompt. I think it's asking for the CTR
--   of the last 7D rolling period. So that's what I've produced.
-- The last week of the period (10/26 to 11/1) have ~1 search per day. So the CTR for those periods
--   likely do not have complete data.

with searches_per_day as (
    select
        searches.searched_at::date as search_date,
        count(1) as searches,
        sum(searches.with_clicks::int) as with_clicks
    from {{ ref('stg_coveo__searches') }} searches
    group by search_date
    order by search_date desc
)

select
    s.search_date,
    s.searches,
    s.with_clicks,
    -- Note we use 6 days preceding because 'current row' is inclusive
    sum(s.searches) over (order by search_date asc rows between 6 preceding and current row) as searches_7DR,
    sum(s.with_clicks) over (order by search_date asc rows between 6 preceding and current row) as with_clicks_7DR,
    with_clicks_7DR / searches_7DR as CTR_7DR
from searches_per_day as s
