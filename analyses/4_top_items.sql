-- List the top 10 most popular items and display their average click rank.

-- Notes:
-- I'm not sure what "item" means - I think that's the `documentUrl` field on the clicks table
-- So the "most popular item" is the `documentUrl` with the most clicks.
-- And the click rank of that item is just the average of the clickRank field.
-- But this is such a simple query that maybe I'm misinterpreting something

select
    document_url,
    count(1) as clicks,
    avg(click_rank) as average_click_rank
from {{ ref('stg_coveo__clicks') }} c
group by document_url
order by clicks desc
limit 10
