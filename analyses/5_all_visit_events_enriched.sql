-- List all events related to visits, in order of date, adding the following information
-- ○ The sequence number (1 to N) of the event over the visit
-- ○ The time difference (in milliseconds) between the event and the previous
-- one. The first event having 0 or null since no previous event

select 
    event_id,
    visit_id,
    row_number() over (partition by visit_id order by sent_at asc) as event_sequence_number,
    sent_at,
    lag(sent_at) over (partition by visit_id order by sent_at asc) as previous_event_sent_at,
    timediff(MILLISECOND, previous_event_sent_at, sent_at) as timediff_previous_event
from {{ ref('stg_coveo__custom_events') }}
qualify count(1) over (partition by visit_id) > 1
order by visit_id, sent_at desc
